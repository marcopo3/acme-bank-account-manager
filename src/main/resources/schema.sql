CREATE TABLE IF NOT EXISTS t_account (
  id int PRIMARY KEY,
  balance DOUBLE DEFAULT 0,
  currency VARCHAR (3) DEFAULT 'HKD'
);