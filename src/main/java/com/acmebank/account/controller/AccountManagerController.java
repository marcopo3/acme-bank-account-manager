package com.acmebank.account.controller;

import com.acmebank.account.model.Account;
import com.acmebank.account.model.TransferResult;
import com.acmebank.account.response.GenericApiResponse;
import com.acmebank.account.controller.param.TransferParam;
import com.acmebank.account.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@Slf4j
public class AccountManagerController {

	@Autowired
	AccountService accountService;

	@GetMapping("/account/{id}")
	public ResponseEntity<GenericApiResponse<Account>> getAccountById(@PathVariable(value = "id") Integer accountId) {
		log.info("/account id = {}", accountId);
		Account account = accountService.getAccountById(accountId);
		GenericApiResponse<Account>  response = new GenericApiResponse(account);
		return ok(response);
	}

	@PostMapping ("/account/transaction")
	public ResponseEntity<GenericApiResponse<TransferResult>> transfer (@RequestBody final TransferParam param){
		log.info("/account/transaction param={}", param);
		TransferResult transferResult = accountService.transfer(param.getSenderAccountId(), param.getReceiverAccountId(), param.getAmount());
		GenericApiResponse<TransferResult>  response = new GenericApiResponse(transferResult);
		return ok(response);
	}
}
