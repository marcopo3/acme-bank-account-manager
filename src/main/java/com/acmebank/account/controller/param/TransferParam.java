package com.acmebank.account.controller.param;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferParam {
    Integer senderAccountId;
    Integer receiverAccountId;
    Double amount;
}
