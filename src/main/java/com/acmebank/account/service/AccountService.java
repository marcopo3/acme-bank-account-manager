package com.acmebank.account.service;

import com.acmebank.account.exception.GenericServiceRuntimeException;
import com.acmebank.account.model.Account;
import com.acmebank.account.constant.GenericExceptionType;
import com.acmebank.account.model.TransferResult;
import com.acmebank.account.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AccountService {

    @Autowired
    private AccountRepository accountRepository;
    public Account getAccountById(Integer accountId) throws GenericServiceRuntimeException {
        Account account = accountRepository.findById(accountId).orElseThrow( () -> new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "Account not found for this id", null, 400));

        return account;
    }

    public TransferResult transfer(Integer senderAccountId, Integer receiverAccountId, Double amount) throws GenericServiceRuntimeException{
        Account senderAccount = accountRepository.findById(senderAccountId).orElseThrow( () -> new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "Account not found for this sender id", null, 400));
        Account receiverAccount = accountRepository.findById(receiverAccountId).orElseThrow( () -> new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "Account not found for this receiver id", null, 400));

        if(senderAccount.getBalance() < amount){
            //throw not enough amount
            throw new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the sender account balance is lower than the transfer amount", null, 400);
        }

        if(senderAccountId.equals(receiverAccountId)){
            //throw sender and receiver cannot be same account
            throw new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the sender account and receiver account cannot be the same", null, 400);
        }

        if(amount <= 0){
            //throw sender and receiver cannot be same account
            throw new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the transfer amount can't be zero or negative number", null, 400);
        }

        senderAccount.setBalance(senderAccount.getBalance()-amount);
        receiverAccount.setBalance(receiverAccount.getBalance()+amount);

        accountRepository.save(receiverAccount);
        accountRepository.save(senderAccount);

        return new TransferResult(senderAccount, receiverAccount);
    }
}
