package com.acmebank.account.handler;

import com.acmebank.account.exception.GenericServiceRuntimeException;
import com.acmebank.account.response.GenericExceptionResponse;
import com.acmebank.account.constant.GenericExceptionType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

import static org.springframework.http.ResponseEntity.badRequest;

@ControllerAdvice
public class GenericControllerExceptionHandler {
    @ExceptionHandler(value = {GenericServiceRuntimeException.class})
    @ResponseBody
    protected ResponseEntity<GenericExceptionResponse> handleGenericServiceRuntimeException(HttpServletResponse res, GenericServiceRuntimeException exception) {
        res.setStatus(exception.getStatus());

        GenericExceptionResponse ger = new GenericExceptionResponse(exception.getGenericExceptionType(), exception.getMessage(), exception.getStatus());
        ResponseEntity<GenericExceptionResponse> response = new ResponseEntity<GenericExceptionResponse>(ger, HttpStatus.valueOf(exception.getStatus()));
        return response;
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    @ResponseBody
    protected ResponseEntity<GenericExceptionResponse> handleIllegalArgumentException(HttpServletResponse res, IllegalArgumentException exception) {
        int status = HttpStatus.BAD_REQUEST.value();
        res.setStatus(status);
        return badRequest().body(new GenericExceptionResponse(GenericExceptionType.INVALID_REQUEST, exception.getMessage(), status));
    }

    @ExceptionHandler(value = {MissingRequestHeaderException.class})
    @ResponseBody
    protected ResponseEntity<GenericExceptionResponse> handleMissingRequestHeaderException(HttpServletResponse res, MissingRequestHeaderException exception) {
        int status = HttpStatus.BAD_REQUEST.value();
        res.setStatus(status);
        return badRequest().body(new GenericExceptionResponse(GenericExceptionType.INVALID_REQUEST, exception.getMessage(), status));
    }

    @ExceptionHandler(value = {MissingServletRequestParameterException.class})
    @ResponseBody
    protected ResponseEntity<GenericExceptionResponse> handleMissingServletRequestParameterException(HttpServletResponse res, MissingServletRequestParameterException exception) {
        int status = HttpStatus.BAD_REQUEST.value();
        res.setStatus(status);
        return badRequest().body(new GenericExceptionResponse(GenericExceptionType.INVALID_REQUEST, exception.getMessage(), status));
    }

    @ExceptionHandler(value = {HttpMessageNotReadableException.class})
    @ResponseBody
    protected ResponseEntity<GenericExceptionResponse> handleHttpMessageNotReadableException(HttpServletResponse res, HttpMessageNotReadableException exception) {
        int status = HttpStatus.BAD_REQUEST.value();
        res.setStatus(status);
        return badRequest().body(new GenericExceptionResponse(GenericExceptionType.INVALID_REQUEST, exception.getMessage(), status));
    }
}
