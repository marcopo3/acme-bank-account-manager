package com.acmebank.account.exception;

import com.acmebank.account.constant.GenericExceptionType;
import lombok.Getter;

@Getter
public class GenericServiceRuntimeException extends RuntimeException {
    private int status;
    private GenericExceptionType genericExceptionType;

    public GenericServiceRuntimeException(GenericExceptionType genericExceptionType, Throwable cause) {
        this(genericExceptionType, cause, 500);
    }

    public GenericServiceRuntimeException(GenericExceptionType genericExceptionType, Throwable cause, int status) {
        super(genericExceptionType.getDescription(), cause);
        this.genericExceptionType = genericExceptionType;
        this.status = status;
    }

    public GenericServiceRuntimeException(GenericExceptionType genericExceptionType, String message, Throwable cause) {
        this(genericExceptionType, message, cause, 500);
    }

    public GenericServiceRuntimeException(GenericExceptionType genericExceptionType, String message, Throwable cause, int status) {
        super(message, cause);
        this.genericExceptionType = genericExceptionType;
        this.status = status;
    }

}