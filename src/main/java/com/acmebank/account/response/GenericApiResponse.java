package com.acmebank.account.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericApiResponse<D> {
    private int status = 200;
    private String message = "SUCCESS";
    private D data;

    public GenericApiResponse(D data) {
        this.data = data;
    }

}
