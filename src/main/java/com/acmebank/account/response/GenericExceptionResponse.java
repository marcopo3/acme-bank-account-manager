package com.acmebank.account.response;

import com.acmebank.account.constant.GenericExceptionType;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GenericExceptionResponse {
    private int status;
    private GenericExceptionType error;
    private String message;

    public GenericExceptionResponse(GenericExceptionType error, String message) {
        this(error, message, 500);
    }

    public GenericExceptionResponse(GenericExceptionType error, String message, int status) {
        this.error = error;
        this.message = message;
        this.status = status;
    }
}