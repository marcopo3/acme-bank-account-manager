package com.acmebank.account.response;

import com.acmebank.account.model.Account;
import com.sun.org.apache.bcel.internal.generic.ACONST_NULL;
import lombok.Data;
import org.springframework.http.ResponseEntity;

import static org.springframework.http.ResponseEntity.badRequest;
import static org.springframework.http.ResponseEntity.ok;

@Data
public class AccountResponse {
    private Integer code;
    private String message;
    private Account account;

    public static ResponseEntity<AccountResponse> okResponseEntity(Account account, Integer code, String msg){
        AccountResponse response = new AccountResponse();
        response.setAccount(account);
        response.setCode(code);
        response.setMessage(msg);
        return ok(response);
    }

    public static ResponseEntity<AccountResponse> errorResponseEntity(Integer code, String msg){
        AccountResponse response = new AccountResponse();
        response.setCode(code);
        response.setMessage(msg);
        return badRequest().body(response);
    }
}
