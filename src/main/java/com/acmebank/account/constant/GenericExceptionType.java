package com.acmebank.account.constant;

import lombok.Getter;

@Getter
public enum GenericExceptionType {

    // Generic error
    INTERNAL_SERVER_ERROR("500", "Internal server error"),
    INVALID_REQUEST("400", "Invalid request"),
    NOT_FOUND("404", "Not found");

    private String code;
    private String description;

    GenericExceptionType(String code, String description) {
        this.description = description;
        this.code = code;
    }
}
