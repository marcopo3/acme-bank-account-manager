package com.acmebank.account.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TransferResult {
    Account senderAccount;
    Account receiverAccount;
}
