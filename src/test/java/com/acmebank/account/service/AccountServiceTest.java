package com.acmebank.account.service;

import com.acmebank.account.constant.GenericExceptionType;
import com.acmebank.account.exception.GenericServiceRuntimeException;
import com.acmebank.account.model.Account;
import com.acmebank.account.model.TransferResult;
import com.acmebank.account.repository.AccountRepository;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {
    @InjectMocks
    AccountService accountService;

    @Mock
    AccountRepository accountRepository;

    private static Account senderAccount;
    private static Account receiverAccount;

    @BeforeAll
    public static void init(){
        senderAccount = new Account(12345678, 1000000d, "HKD");
        receiverAccount = new Account(88888888, 1000000d, "HKD");
    }

    @Test
    void getAccountSuccessCase(){
        given(accountRepository.findById(12345678)).willReturn(Optional.of(senderAccount));

        Account returnedAccount = accountService.getAccountById(12345678);

        assertThat(returnedAccount).isNotNull();
        assertThat(returnedAccount).isEqualTo(senderAccount);

    }

    @Test
    void AccountNotFoundCase(){
        //given(accountRepository.findById(123456789)).willThrow(new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "Account not found for this id", null, 400));

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountService.getAccountById(123456789);
        });

        String expectedMessage = "Account not found for this id";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    void transferMoneySuccessCase(){

        given(accountRepository.findById(12345678)).willReturn(Optional.of(senderAccount));
        given(accountRepository.findById(88888888)).willReturn(Optional.of(receiverAccount));

        TransferResult returnedTransfer = accountService.transfer(12345678, 88888888, 100d);

        TransferResult expectedResult = new TransferResult(new Account(12345678,999900d, "HKD"), new Account(88888888,1000100d, "HKD"));
        assertThat(returnedTransfer).isEqualTo(expectedResult);
    }

    @Test
    void notEnoughMoneyCase(){

        given(accountRepository.findById(12345678)).willReturn(Optional.of(senderAccount));
        given(accountRepository.findById(88888888)).willReturn(Optional.of(receiverAccount));

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountService.transfer(12345678, 88888888, 2000000d);
        });

        String expectedMessage = "the sender account balance is lower than the transfer amount";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    void zeroOrNegativeAmountCase(){

        given(accountRepository.findById(12345678)).willReturn(Optional.of(senderAccount));
        given(accountRepository.findById(88888888)).willReturn(Optional.of(receiverAccount));

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountService.transfer(12345678, 88888888, -2000d);
        });

        String expectedMessage = "the transfer amount can't be zero or negative number";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    void sameAccountCase(){

        given(accountRepository.findById(12345678)).willReturn(Optional.of(senderAccount));
        //  given(accountRepository.findById(88888888)).willReturn(Optional.of(receiverAccount));

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountService.transfer(12345678, 12345678, 100d);
        });

        String expectedMessage = "the sender account and receiver account cannot be the same";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }
}
