package com.acmebank.account.controller;
import com.acmebank.account.constant.GenericExceptionType;
import com.acmebank.account.controller.param.TransferParam;
import com.acmebank.account.exception.GenericServiceRuntimeException;
import com.acmebank.account.model.Account;
import com.acmebank.account.model.TransferResult;
import com.acmebank.account.response.GenericApiResponse;
import com.acmebank.account.service.AccountService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
public class AccountManagerControllerTest {
    @InjectMocks
    AccountManagerController accountManagerController;
    @Mock
    AccountService accountService;

    private static Account senderAccount;
    private static Account receiverAccount;
    private static Account postTransferSenderAccount;
    private static Account postTransferReceiverAccount;

    @BeforeAll
    public static void init() {
        senderAccount = new Account(12345678, 1000000d, "HKD");
        receiverAccount = new Account(88888888, 1000000d, "HKD");
        postTransferSenderAccount = new Account(12345678, 999900d, "HKD");
        postTransferReceiverAccount = new Account(88888888, 1000100d, "HKD");
    }

    @Test
    public void successfulGetAccount(){
        given(accountService.getAccountById(12345678)).willReturn(senderAccount);

        ResponseEntity<GenericApiResponse<Account>> response = accountManagerController.getAccountById(12345678);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getData()).isNotNull();
    }

    @Test
    void AccountNotFoundCase(){
        given(accountService.getAccountById(123456789)).willThrow(new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "Account not found for this id", null, 400));

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountManagerController.getAccountById(123456789);
        });

        String expectedMessage = "Account not found for this id";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    public void successfulTransferMoney(){
        given(accountService.transfer(12345678, 88888888, 100d)).willReturn( new TransferResult(postTransferSenderAccount, postTransferReceiverAccount));

        TransferParam param = new TransferParam(12345678, 88888888, 100d);
        ResponseEntity<GenericApiResponse<TransferResult>> response = accountManagerController.transfer(param);

        assertThat(response).isNotNull();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getData()).isNotNull();
    }

    @Test
    void notEnoughMoneyCase(){
        given(accountService.transfer(12345678, 88888888, 2000000d)).willThrow(new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the sender account balance is lower than the transfer amount", null, 400));

        TransferParam param = new TransferParam(12345678, 88888888, 2000000d);

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountManagerController.transfer(param);
        });

        String expectedMessage = "the sender account balance is lower than the transfer amount";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    void zeroOrNegativeAmountCase(){
        given(accountService.transfer(12345678, 88888888, -200d)).willThrow(new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the transfer amount can't be zero or negative number", null, 400));

        TransferParam param = new TransferParam(12345678, 88888888, -200d);

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountManagerController.transfer(param);
        });

        String expectedMessage = "the transfer amount can't be zero or negative number";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }

    @Test
    void sameAccountCase(){
        given(accountService.transfer(12345678, 12345678, 100d)).willThrow(new GenericServiceRuntimeException(GenericExceptionType.INVALID_REQUEST, "the sender account and receiver account cannot be the same", null, 400));

        TransferParam param = new TransferParam(12345678, 12345678, 100d);

        GenericServiceRuntimeException exception = assertThrows(GenericServiceRuntimeException.class, () -> {
            accountManagerController.transfer(param);
        });

        String expectedMessage = "the sender account and receiver account cannot be the same";
        String returnedMessage = exception.getMessage();
        int expectedHttpCode = 400;
        int returnedHttpCode = exception.getStatus();

        assertThat(returnedMessage).isEqualTo(expectedMessage);
        assertThat(returnedHttpCode).isEqualTo(expectedHttpCode);
    }
}
