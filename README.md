# ACMEBank Account Manager
## Build project

desired environment:
- Java 8 or above

1. In command console, go to the root folder of this project

2. Please run this command:
   `./gradlew clean build`

------------
## Run project
After building the project,
please use this command to run the project:

`java -jar build/libs/account-manager-1.0.0.jar`

------------

## Run test
1. In command console, go to the root folder of this project

2. Please run this command:
   `./gradlew test`

3. Test report will be available on
   `build/reports/tests/test/index.html`
------------

##  API Endpoint
### Retrieve Account information
`GET` http://localhost:8080/account/{accountId}
   
example: http://localhost:8080/account/12345678

sample response body

200 OK
```
{
    "status": 200,
    "message": "SUCCESS",
    "data": {
        "id": 12345678,
        "balance": 999700.0,
        "currency": "HKD"
    }
}
```
400 Bad Request
```aidl
{
    "status": 400,
    "error": "INVALID_REQUEST",
    "message": "Account not found for this id"
}
```

### Transfer money
      
`POST` http://localhost:8080/account/transaction

sample request body

```
{
   "senderAccountId" : 12345678,
   "receiverAccountId" : 88888888,
   "amount" : 100
}
```

sample response body

200 OK

```
{
    "senderAccountId" : 12345678,
    "receiverAccountId" : 88888888,
    "amount" : 100
}
```

400 bad request

```
{
    "status": 400,
    "error": "INVALID_REQUEST",
    "message": "the sender account balance is lower than the transfer amount"
}
```

